MAKEFILE_DIR = $(dir $(realpath $(firstword $(MAKEFILE_LIST))))

RTEMS_VERSION = 6
PREFIX = $(MAKEFILE_DIR)/rtems/$(RTEMS_VERSION)
RSB = $(MAKEFILE_DIR)/external/rtems-source-builder
SRC_RTEMS = $(MAKEFILE_DIR)/external/rtems

export PATH := $(PREFIX)/bin:$(PATH)

.PHONY: help
help:
	@##H## Show some help.
	@ grep -B 1 '@##H##' $(firstword $(MAKEFILE_LIST)) \
		| grep -v grep | grep -v -- "--" | sed -e 's/@##H##//g'

.PHONY: install
install: submodule-update toolchain bsp
	@##H## build and install the toolchain and BSP

.PHONY: submodule-update
submodule-update:
	@##H## initialize the submodules
	git submodule update --init

.PHONY: toolchain
toolchain:
	@##H## build the toolchain
	rm -rf $(RSB)/rtems/build
	cd $(RSB)/rtems && ../source-builder/sb-set-builder \
	    --prefix=$(PREFIX) \
	    --log=$(RSB)/b-rsb-toolchain-arm.log \
	    $(RTEMS_VERSION)/rtems-arm
	rm -rf $(RSB)/rtems/build

.PHONY: bsp
bsp:
	@##H## build the bsp
	cd $(SRC_RTEMS) && ./waf clean || true
	cd $(SRC_RTEMS) && ./waf bsp_defaults --rtems-bsps=arm/stm32f105rc > config.ini
	#cd $(SRC_RTEMS) && sed -i \
	#	-e "s|RTEMS_POSIX_API = False|RTEMS_POSIX_API = True|" \
	#	-e "s|BUILD_TESTS = False|BUILD_TESTS = True|" \
	#	config.ini
	cd $(SRC_RTEMS) && ./waf configure --prefix=$(PREFIX)
	cd $(SRC_RTEMS) && ./waf
	cd $(SRC_RTEMS) && ./waf install

.PHONY: tags
tags:
	@##H## create ctags
	rm -f $@
	ctags -f $@ -a \
		--extra=+f \
		--extra=+q \
		--recurse=yes \
		--languages=+Asm \
		--languages=+C \
		--languages=+C++ \
		--languages=+Python \
		--if0=yes \
		--c-kinds=+fgptx \
		--c++-kinds=+fgptx \
		--Asm-kinds=+dlmt \
		--exclude="obj-*" \
		--exclude="build" \
		--exclude="freebsd-org" \
		--exclude="rtems-source-builder" \
		--exclude="gui-simulator" \
		.

.PHONY: shell
shell:
	@##H## start a shell
	PREPROMPT="(RRR)" zsh
